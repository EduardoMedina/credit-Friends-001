//
//  CFRequestViewController.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 10/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import  UIKit
import Firebase
import FirebaseDatabase

class CFRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let user = Auth.auth().currentUser?.uid
    //list to store all the request
    var otherRequestsList = [CFRequestModel]()
    @IBOutlet weak var tableViewARecord: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Solicitudes de usuarios"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //getting a reference to the node request
        let refOtherRequest: DatabaseReference!
        refOtherRequest = Database.database().reference()
        
        CFRequestClass().initWithData(0,(Auth.auth().currentUser?.uid)!,IdentifiersViewController.Request,completion:{(resultList : [CFRequestModel]) in
            print(resultList)
            self.otherRequestsList = resultList
            self.tableViewARecord.reloadData()
        })
        
        CFRequestClass().requestEventListeningBD(0,(Auth.auth().currentUser?.uid)!,IdentifiersViewController.Request,refRequest: refOtherRequest, completion:{(resultList : [CFRequestModel]) in
            print(resultList)
            self.otherRequestsList = resultList
            self.tableViewARecord.reloadData()
       })
        
    }

    //MARK: TableView Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return otherRequestsList.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        //creating a cell using the custom class
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CFRequestsTableViewCell
        
        //the requests object
        let requests: CFRequestModel
        
        //getting the requests of selected position
        requests = otherRequestsList[indexPath.row]
        
        //adding values to labels
        cell.lblAmount.text = "$ " + String(format: "%.2f", Double(requests.amount)!) + " MXN"
        cell.lblInstallments.text = String(format: "%.0f", Double(requests.installment)!)  + " Meses"
        cell.lblInterest.text = String(format: "%.2f", Double(requests.interest)!) + "%"
        cell.lblCreatedAt.text = requests.createdAt
        cell.lblStatus.backgroundColor = Int(requests.status) == 1 ? UIColor.yellow : Int(requests.status) == 2 ? UIColor.green : UIColor.red
        cell.lblStatus.textColor = Int(requests.status) == 3 ? UIColor.white : UIColor.black
        cell.lblTotal.text = "$ " + String(format: "%.2f", Double(requests.total)!) + " MXN"
        
        //returning cell
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let requests: CFRequestModel
        //getting the requests of selected position
        requests = otherRequestsList[indexPath.row]
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "" + IdentifiersViewController.ValidateRequest.ToString) as! CFValidateRequestViewController
        vc.validateRequestsList = requests
        appRootViewController().present(vc, animated: true, completion: nil)
    }
    
    
}
