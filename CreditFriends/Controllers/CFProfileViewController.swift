//
//  CFProfileViewController.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 10/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import FirebaseAuth


class CFProfileViewController : UIViewController, UITableViewDelegate, UITableViewDataSource,CFNavigationDelegate{
    
    //list to store all the request
    var  myRequestsList = [CFRequestModel]()
    var email : String = ""
    var userId : String = ""
    var typeProfile : Int = 1
    @IBOutlet weak var tableViewARecord: UITableView!
    @IBAction func btnDismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnDismiss: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Perfil de usuario"
        
        if email == ""{
            if Auth.auth().currentUser?.displayName == nil
            {lblEmail.text = "Desconocido"}
            else{

            }
        }else{
            lblEmail.text = email
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        btnDismiss.isHidden = typeProfile == 1 ? true : false
        let refMyRequest: DatabaseReference!
        refMyRequest = Database.database().reference()

        CFRequestClass().initWithData(typeProfile,userId,IdentifiersViewController.Profile,
                                      completion:{(resultList : [CFRequestModel]) in
            print(resultList)
            self.myRequestsList = resultList
            self.tableViewARecord.reloadData()
        })

        CFRequestClass().requestEventListeningBD(typeProfile,userId,IdentifiersViewController.Profile,
                                                 refRequest: refMyRequest,
                                                 completion:{(resultList : [CFRequestModel]) in
            print(resultList)
            self.myRequestsList = resultList
            self.tableViewARecord.reloadData()
        })
    }
    
    //MARK: TableView Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return myRequestsList.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        //creating a cell using the custom class
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CFRequestsTableViewCell
        
        //the requests object
        let requests: CFRequestModel
        
        //getting the requests of selected position
        requests = myRequestsList[indexPath.row]
        
        //adding values to labels

        cell.lblAmount.text =  "$ " + String(format: "%.2f", Double(requests.amount)!) + " MXN"
        cell.lblInstallments.text = String(format: "%.0f", Double(requests.installment)!)  + " Meses"
        cell.lblInterest.text = String(format: "%.2f", Double(requests.interest)!) + "%"
        cell.lblCreatedAt.text = requests.createdAt
        cell.lblStatus.backgroundColor = Int(requests.status) == 1 ? UIColor.yellow : Int(requests.status) == 2 ? UIColor.green : UIColor.red
        cell.lblStatus.textColor = Int(requests.status) == 3 ? UIColor.white : UIColor.black
        cell.lblTotal.text = "$ " + String(format: "%.2f", Double(requests.total)!) + " MXN"
        //returning cell
        return cell
    }
    
}
