//
//  CFNewRequestViewController.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 10/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase

class CFNewRequestViewController: UIViewController {
    let user = Auth.auth().currentUser
    @IBAction func btnSend(_ sender: UIButton) {
        
        if txtTotalWithInterest.text != "" {
            addRequest()
        }
    }
    
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtTotalWithInterest: UITextField!
    @IBOutlet weak var scInstallments: UISegmentedControl!
    
    var kInstallments = 0
    var kInterest = 0.0
    var kAmount = 0.0
    var totalToPay = 0.0
    var iva = 16.0
   
    @IBAction func sgAction(_ sender: UISegmentedControl) {
        
        /*Calculate total to pay with taxes*/
        if txtAmount.text != "" {
            kAmount = Double(txtAmount.text!)!
            kInstallments = (sender.selectedSegmentIndex == 0 ? 3 : sender.selectedSegmentIndex == 1 ? 6 : 9)
            kInterest = (sender.selectedSegmentIndex == 0 ? 5.0 : sender.selectedSegmentIndex == 1 ? 7.0 : 12.0)
            
            let kIva = (iva / 100.0);
            
            let percentage :Double = (kInterest / 100.0);
            
            let residualPercentage = kAmount * percentage
            
            let finalPercentage = residualPercentage * Double(kInstallments)
            
            totalToPay = finalPercentage + kAmount
            
            totalToPay = (totalToPay * kIva) + totalToPay
            
            txtTotalWithInterest.text =  "$ " + String(format: "%.2f", totalToPay) + " MXN"
        }
    }
    
    //defining firebase reference var
    var refRequest: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Nueva solicitud"
        self.txtAmount.becomeFirstResponder()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func addRequest(){
        
        refRequest = Database.database().reference()
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let resultNow = formatter.string(from: date)
        
        if txtAmount.text != "" {
            //generating a new key inside request node
            //and also getting the generated key
            let key = refRequest.childByAutoId().key
            //creating request with the given values
            let request = ["id":key as Any,
                            "amount": kAmount as Double,
                            "installments": kInstallments  as Int,
                            "interest":  kInterest as Double,
                            "createdAt" :resultNow as String,
                            "updatedAt" :resultNow as String,
                            "status" : 1 as Int,
                            "description" : "Nueva Solicitud" as String,
                            "currency" : 1 as Int,
                            "iva" : iva as Double,
                            "total" : totalToPay as Double,
                            "userId" : user?.uid as Any
            ]
            //adding the request inside the generated unique key
            refRequest.child("requests").child(key).setValue(request)
            
            //displaying message
            print("Add solicitud")
            self.navigationController?.popViewController(animated: true)
           
        }
    }
}
