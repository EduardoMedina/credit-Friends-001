//
//  CFRecordViewController.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 10/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import  UIKit
import Firebase
import FirebaseDatabase

class CFRecordViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    //list to store all the request
    var requestsList = [CFRequestModel]()
    @IBOutlet weak var tableViewARecord: UITableView!
    
    @IBAction func btnNewRequest(_ sender: UIButton) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "" + IdentifiersViewController.NewRequest.ToString) as! CFNewRequestViewController
        self.navigationController?.title = "Nueva Solicitud"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Mi Historial"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //getting a reference to the node request
        let refRequest: DatabaseReference!
        refRequest = Database.database().reference()
        
        /* Get the stored data, we perform the search based on the user id and in a closure show them in our table*/
        CFRequestClass().initWithData(0,(Auth.auth().currentUser?.uid)!,IdentifiersViewController.Record,completion:{(resultList : [CFRequestModel]) in
            print(resultList)
            self.requestsList = resultList
            self.tableViewARecord.reloadData()
        })
        
        
        CFRequestClass().requestEventListeningBD(0,(Auth.auth().currentUser?.uid)!,IdentifiersViewController.Record,refRequest: refRequest, completion:{(resultList : [CFRequestModel]) in
            print(resultList)
            self.requestsList = resultList
            self.tableViewARecord.reloadData()
        })
    }
    
    //MARK: TableView Delegate
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return requestsList.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        //creating a cell using the custom class
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CFRequestsTableViewCell
        
        //the requests object
        let requests: CFRequestModel
        
        //getting the requests of selected position
        requests = requestsList[indexPath.row]
        
        //adding values to labels
        cell.lblAmount.text =  "$ " + String(format: "%.2f", Double(requests.amount)!) + " MXN"
        cell.lblInstallments.text = String(format: "%.0f", Double(requests.installment)!)  + " Meses"
        cell.lblInterest.text = String(format: "%.2f", Double(requests.interest)!) + "%"
        cell.lblCreatedAt.text = requests.createdAt
        cell.lblStatus.backgroundColor = Int(requests.status) == 1 ? UIColor.yellow : Int(requests.status) == 2 ? UIColor.green : UIColor.red
        cell.lblStatus.textColor = Int(requests.status) == 3 ? UIColor.white : UIColor.black
        cell.lblTotal.text = "$ " + String(format: "%.2f", Double(requests.total)!) + " MXN"

        return cell
    }
    
    

}
