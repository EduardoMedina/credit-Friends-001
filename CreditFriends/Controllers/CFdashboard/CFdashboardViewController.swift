//
//  CFdashboardViewController.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 09/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

class CFdashboardViewController: UIViewController ,CFNavigationDelegate {
    @IBOutlet weak var lblUserName: UILabel!
    let navButtonsDelegate = CFNavigationsButton()

    @IBAction func btnProfile(_ sender: UIButton) {
        // go to my profile
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "" + IdentifiersViewController.Profile.ToString) as! CFProfileViewController
        vc.userId = (Auth.auth().currentUser?.uid)!
        vc.email = (Auth.auth().currentUser?.displayName)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnRecord(_ sender: UIButton) {
    
        // go to my record
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "" + IdentifiersViewController.Record.ToString) as! CFRecordViewController
        
        self.navigationController?.pushViewController(vc, animated: true)


    }
    @IBAction func btnRequest(_ sender: UIButton) {
        // go to my users requests
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "" + IdentifiersViewController.Request.ToString) as! CFRequestViewController
        
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    override func viewDidLoad() {
        super .viewDidLoad()
        self.title = "Dashboard"
        self.navButtonsDelegate.navigationDelegate = self as CFNavigationDelegate
        self.navButtonsDelegate.CFSetRightBarButtonItems(self)

        if Auth.auth().currentUser?.displayName == nil {lblUserName.text = "Bienvenido: " + "Desconocido"}else{
            lblUserName.text = "Bienvenido: " + (Auth.auth().currentUser?.displayName)!

        }
        
    }
    func CFSearchViewController() {
        UserService().CloseSession()
    }
    
}

