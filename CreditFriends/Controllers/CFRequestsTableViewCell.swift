//
//  CFRequestsTableViewCell.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 10/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//
import  Foundation
import UIKit

class CFRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblInstallments: UILabel!
    @IBOutlet weak var lblInterest: UILabel!
    @IBOutlet weak var lblCreatedAt: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var btnValidateNow: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
