//
//  CFValidateRequestViewController.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 11/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase

class CFValidateRequestViewController : UIViewController{
    var validateRequestsList = CFRequestModel()
    
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblInstallments: UILabel!
    @IBOutlet weak var lblInterest: UILabel!
    @IBOutlet weak var lblCreatedAt: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    /*
     
        We visualize the user's history, we send what type of profile we are requested.
     
        typeProfile
         
        1 = my user profile, we show the pending requests
        2 = user profile, we show the approved or rejected requests.
     
     */
    @IBAction func btnSeeMore(_ sender: UIButton) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: IdentifiersViewController.Profile.ToString) as! CFProfileViewController
        vc.email = validateRequestsList.userId;
        vc.userId = validateRequestsList.userId
        vc.typeProfile = 2
        appRootViewController().present(vc, animated: true, completion: nil)
    }
    
    var indexSelected = 0
    @IBAction func sgValidate(_ sender: UISegmentedControl) {
        
        indexSelected = sender.selectedSegmentIndex
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSendNow(_ sender: UIButton) {
        
        //
        let refValidateRequest: DatabaseReference!
        refValidateRequest = Database.database().reference()
        
        // change the status of the selected application, based on the id. single regiment in the BD
        refValidateRequest.child("requests").child(validateRequestsList.idUnique).updateChildValues(["status":indexSelected == 0 ? 2 : 3 ])
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblUser.text = validateRequestsList.userId
        lblAmount.text = "$ " + String(format: "%.2f", Double(validateRequestsList.amount)!) + " MXN"
        lblInstallments.text = String(format: "%.0f", Double(validateRequestsList.installment)!) + " Meses"
        lblInterest.text = String(format: "%.2f", Double(validateRequestsList.interest)!) + " %"
        lblCreatedAt.text = validateRequestsList.createdAt
        lblTotal.text = "$ " + String(format: "%.2f", Double(validateRequestsList.total)!) + " MXN"
    }
}
