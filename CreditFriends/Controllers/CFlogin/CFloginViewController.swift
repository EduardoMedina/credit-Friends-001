//
//  CFloginViewController.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 09/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth

class CFloginViewController: UIViewController {
    
    var isUserLogin = true
    @IBOutlet weak var txtEmail: UITextField!

   
    @IBAction func scOptions(_ sender: UISegmentedControl) {
      
        isUserLogin = sender.selectedSegmentIndex == 0 ? true : false;
    }
    @IBAction func btnLogin(_ sender: ButtonLogin) {
        let email = txtEmail.text
        self.SaveUserWithData(email!,isLogin: isUserLogin)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtEmail.becomeFirstResponder()
        self.title = "Inicio de sesión"
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func SaveUserWithData(_ email:String,isLogin : Bool){
        
        // Do some form validation on the email and password
        if email != "" {
            if(isLogin){
                Auth.auth().signIn(withEmail: email, password: "123456", completion: {(user, error) in
                    // Check that user isn´t nil
                    if let _ = user {
                        // user is found
                        guard error == nil else{
                            return
                        }
                        guard let user = user else{return}
                        let changeRequest = user.createProfileChangeRequest()
                        changeRequest.displayName = email
                        
                        changeRequest.commitChanges(completion: {(error) in
                            guard error == nil else{
                                return
                            }
                        })

                        UserService().storeLoginReady()
                        
                        if UserService().searchStoreLoginReady(){
                            
                            TTGetDelegate().goToView(IdentifiersViewController.Dashboard.ToString,
                                                     transitionType: kCATransitionFade,
                                                     WithNav: true)
                        }else{
                            
                            TTGetDelegate().goToView(IdentifiersViewController.LoginView.ToString,
                                                     transitionType: kCATransitionFade,
                                                     WithNav: true)
                        }
                        
                        
                        
                    }else
                    {
                        // user not found
                       
                    }
                })

            }else{
                Auth.auth().createUser(withEmail: email, password: "123456", completion: {(user, error) in
                    guard error == nil else{
                        return
                    }
                    guard let user = user else{return}
                    let changeRequest = user.createProfileChangeRequest()
                    changeRequest.displayName = email
                    changeRequest.commitChanges(completion: {(error) in
                        guard error == nil else{
                            return
                        }
                    })
                    // user is found
                    UserService().storeLoginReady()
                    
                    if UserService().searchStoreLoginReady(){
                        
                        TTGetDelegate().goToView(IdentifiersViewController.Dashboard.ToString,
                                                 transitionType: kCATransitionFade,
                                                 WithNav: true)
                    }else{
                        
                        TTGetDelegate().goToView(IdentifiersViewController.LoginView.ToString,
                                                 transitionType: kCATransitionFade,
                                                 WithNav: true)
                    }
                    
                
                })
            }
        }else{
            
        }
        
    }
    
}

