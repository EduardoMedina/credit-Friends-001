//
//  CFCurrentUserDefault.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 09/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

let screenSize = UIScreen.main.bounds.size
class UserService {
    let KeyUserLoginID = "UserIsLogin"
    

    
    //MARK: Login
    
    //save
    func storeLoginReady() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: KeyUserLoginID)
    }
    
    // search
    func searchStoreLoginReady() -> Bool {
        
        let userDefaults = UserDefaults.standard
        
        guard case let LoginSearch as Bool = userDefaults.value(forKey: KeyUserLoginID) else {
            userDefaults.set(false, forKey: KeyUserLoginID)
            return false
        }
        
        return LoginSearch
    }
    
    //Close session
    func CloseSession(){
        
        UserDefaults.standard.removeObject(forKey: KeyUserLoginID)
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        // Delete all object in local storage
        LocalStorage().TTDeleteAllObjects()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: IdentifiersViewController.LoginView.ToString)
        CFLayerAnimated((UIApplication.shared.keyWindow?.layer)!,transitionType: kCATransitionFromLeft)
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    
}

