//
//  CFUserRequests.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 10/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import RealmSwift
import Firebase
import FirebaseDatabase
import UIKit

class  CFRequestModel: Object {
    dynamic var id          = ""
    dynamic var amount      = ""
    dynamic var currency      = ""
    dynamic var createdAt   = ""
    dynamic var descriptionReq      = ""
    dynamic var idUnique        = ""
    dynamic var installment        = ""
    dynamic var interest            = ""
    dynamic var status          = ""
    dynamic var updateAt        = ""
    dynamic var userId          = ""
    dynamic var total           = ""
     dynamic var iva            = ""
    

    
    override static func primaryKey() -> String {
        return "id"
    }

    func  InitModel(amount : Double,
                    createdAt : String,
                    currency : Int,
                    descriptionReq : String,
                    idUnique : String,
                    installment : Int,
                    interest : Double,
                    status : Int,
                    updateAt : String,
                    userId : String,
                    total : Double,
                    iva : Double) -> CFRequestModel {
        
        let  requestModel  = CFRequestModel()
        
        requestModel.amount = String(format: "%f",amount)
        requestModel.createdAt = createdAt
        requestModel.currency = String(format:"%d",currency)
        requestModel.descriptionReq = descriptionReq
        requestModel.idUnique = idUnique
        requestModel.installment = String(format:"%d",installment)
        requestModel.interest = String(format:"%f",interest)
        requestModel.status = String(format:"%d",status)
        requestModel.updateAt = updateAt
        requestModel.userId = userId
        requestModel.total = String(format:"%f",total)
        requestModel.iva = String(format:"%f",iva)
        
        let myReq = searchReqLogged()
        if myReq == nil {
            requestModel.id = "0"
            return requestModel
        }
        
        let tmpIdInt = Int((myReq?.id)!)! + 1
        
        requestModel.id = "\(tmpIdInt)"
        
        return requestModel
        
    }
    func searchReqLogged() -> CFRequestModel? {
        
        let myReq = LocalStorage().TTGetObjectWithPredicate(CFRequestModel().self, WithPredicate: false, "").last
        
        return myReq;
    }
    
    func  UpdateReqModel(amount : Double,
                         createdAt : String,
                         currency : Int,
                         descriptionReq : String,
                         idUnique : String,
                         installment : Int,
                         interest : Double,
                         status : Int,
                         updateAt : String,
                         userId : String,
                         total : Double,
                         iva : Double) -> CFRequestModel {
        
        
        
        let resultModel = LocalStorage().TTGetObjectWithPredicate(CFRequestModel().self, WithPredicate: true, "idUnique='\(idUnique)'").last
        
        let realm = try! Realm()
        
        realm.beginWrite()
        
        resultModel?.amount = String(format: "%f",amount)
        resultModel?.currency = String(format:"%d",currency)
        resultModel?.descriptionReq = descriptionReq
        resultModel?.installment = String(format:"%d",installment)
        resultModel?.interest = String(format:"%f",interest)
        resultModel?.status = String(format:"%d",status)
        resultModel?.total = String(format:"%f",total)
        resultModel?.iva = String(format:"%f",iva)
        resultModel?.updateAt = updateAt

        try! realm.commitWrite()
        
        
        return resultModel!
        
    }
}

class CFRequestClass {
    
    var otherRequestsList = [CFRequestModel]()
    func requestEventListeningBD(_ typeProfile:Int,_ userId : String, _ typeRequest : IdentifiersViewController,refRequest : DatabaseReference, completion : @escaping (_ result: [CFRequestModel]) -> Void) {
        
        refRequest.child("requests").observeSingleEvent(of: .value, with:{ (snapshot) in
            // Get requests value
            for Request in snapshot.children.allObjects as! [DataSnapshot] {
                //getting values
                let kRequestObject = Request.value as? [String: AnyObject]
                let kAmount  = kRequestObject?["amount"] as! Double
                let kCreatedAt  = kRequestObject?["createdAt"] as! String
                let KUserId  = kRequestObject?["userId"] as! String
                let kCurrency  = kRequestObject?["currency"] as! Int
                let kDescription  = kRequestObject?["description"] as! String
                let kId  = kRequestObject?["id"] as! String
                let kInstallment  = kRequestObject?["installments"] as! Int
                let kInterest  = kRequestObject?["interest"] as! Double
                let kStatus  = kRequestObject?["status"] as! Int
                let kUpdatedAt  = kRequestObject?["updatedAt"] as! String
                
                let kIva  = kRequestObject?["iva"] as! Double
                let kTotal  = kRequestObject?["total"] as! Double
                if (LocalStorage().TTCountObjectsSelectedWithPredicate(CFRequestModel(),
                                                                       WithPredicate: true,
                                                                       StringPredicate: "idUnique='\(kId)'") == 0){
                    
                    LocalStorage().TTAddObject(CFRequestModel().InitModel(amount: kAmount,
                                                                          createdAt: kCreatedAt,
                                                                          currency: kCurrency,
                                                                          descriptionReq: kDescription,
                                                                          idUnique: kId,
                                                                          installment: kInstallment,
                                                                          interest: kInterest,
                                                                          status: kStatus,
                                                                          updateAt: kUpdatedAt,
                                                                          userId: KUserId,total: kTotal, iva:kIva))
                }else{
                    LocalStorage().TTUpdateObject(CFRequestModel().UpdateReqModel( amount: kAmount,
                                                                                   createdAt: kCreatedAt,
                                                                                   currency: kCurrency,
                                                                                   descriptionReq: kDescription,
                                                                                   idUnique: kId,
                                                                                   installment: kInstallment,
                                                                                   interest: kInterest,
                                                                                   status: kStatus,
                                                                                   updateAt: kUpdatedAt,
                                                                                   userId: KUserId,total: kTotal, iva:kIva))
                }
            }
            
            self.initWithData(typeProfile,userId,typeRequest,completion: completion )

        }) { (error) in
            print(error.localizedDescription)
        }
        
        refRequest.child("requests").observe(.childChanged, with:{ (snapshot) in
            // Get requests value
            let Request = snapshot.value
            //getting values
            let kRequestObject = Request as? NSDictionary
            let kAmount  = kRequestObject?["amount"] as! Double
            let kCreatedAt  = kRequestObject?["createdAt"] as! String
            let KUserId  = kRequestObject?["userId"] as! String
            let kCurrency  = kRequestObject?["currency"] as! Int
            let kDescription  = kRequestObject?["description"] as! String
            let kId  = kRequestObject?["id"] as! String
            let kInstallment  = kRequestObject?["installments"] as! Int
            let kInterest  = kRequestObject?["interest"] as! Double
            
            let kStatus  = kRequestObject?["status"] as! Int
            let kUpdatedAt  = kRequestObject?["updatedAt"] as! String
            
            let kIva  = kRequestObject?["iva"] as! Double
            let kTotal  = kRequestObject?["total"] as! Double
            if (LocalStorage().TTCountObjectsSelectedWithPredicate(CFRequestModel(),
                                                                   WithPredicate: true,
                                                                   StringPredicate: "idUnique='\(kId)'") > 0){
                LocalStorage().TTUpdateObject(CFRequestModel().UpdateReqModel(amount: kAmount,
                                                                              createdAt: kCreatedAt,
                                                                              currency: kCurrency,
                                                                              descriptionReq: kDescription,
                                                                              idUnique: kId,
                                                                              installment: kInstallment,
                                                                              interest: kInterest,
                                                                              status: kStatus,
                                                                              updateAt: kUpdatedAt,
                                                                              userId: KUserId,total: kTotal, iva:kIva ))
            }else{
                
            }
            self.initWithData(typeProfile,userId,typeRequest,completion:completion )
        }) { (error) in
            print(error.localizedDescription)
        }
        
        refRequest.child("requests").observe(.childAdded, with:{ (snapshot) in
            // Get requests value
            let Request = snapshot.value
            //getting values
            let kRequestObject = Request as? NSDictionary
            let kAmount  = kRequestObject?["amount"] as! Double
            let kCreatedAt  = kRequestObject?["createdAt"] as! String
            let KUserId  = kRequestObject?["userId"] as! String
            let kCurrency  = kRequestObject?["currency"] as! Int
            let kDescription  = kRequestObject?["description"] as! String
            let kId  = kRequestObject?["id"] as! String
            let kInstallment  = kRequestObject?["installments"] as! Int
            let kInterest  = kRequestObject?["interest"] as! Double
            
            let kStatus  = kRequestObject?["status"] as! Int
            let kUpdatedAt  = kRequestObject?["updatedAt"] as! String
            
            let kIva  = kRequestObject?["iva"] as! Double
            let kTotal  = kRequestObject?["total"] as! Double
            if (LocalStorage().TTCountObjectsSelectedWithPredicate(CFRequestModel(),
                                                                   WithPredicate: true,
                                                                   StringPredicate: "idUnique='\(kId)'") == 0){
                
                LocalStorage().TTAddObject(CFRequestModel().InitModel(amount: kAmount,
                                                                      createdAt: kCreatedAt,
                                                                      currency: kCurrency,
                                                                      descriptionReq: kDescription,
                                                                      idUnique: kId,
                                                                      installment: kInstallment,
                                                                      interest: kInterest,
                                                                      status: kStatus,
                                                                      updateAt: kUpdatedAt,
                                                                      userId: KUserId,total: kTotal, iva:kIva))
            }else{
                LocalStorage().TTUpdateObject(CFRequestModel().UpdateReqModel( amount: kAmount,
                                                                               createdAt: kCreatedAt,
                                                                               currency: kCurrency,
                                                                               descriptionReq: kDescription,
                                                                               idUnique: kId,
                                                                               installment: kInstallment,
                                                                               interest: kInterest,
                                                                               status: kStatus,
                                                                               updateAt: kUpdatedAt,
                                                                               userId: KUserId,total: kTotal, iva:kIva))
            }
            
            self.initWithData(typeProfile,userId,typeRequest, completion: completion)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    // Get Data From local storage
    func initWithData(_ typeProfile :Int,_ userId : String,_ typeRequest : IdentifiersViewController,completion : (_ result: [CFRequestModel]) -> Void){
        

        let resultReqModel = LocalStorage().TTGetObjectWithPredicate(CFRequestModel().self,
                                                                     WithPredicate: true,
                                                                     (typeRequest == IdentifiersViewController.Profile ? typeProfile == 1 ? "userId =='\(userId)' && status='\(1)'" : "userId =='\(userId)' && (status='\(2)' || status='\(3)')"
                                                                        : typeRequest == IdentifiersViewController.Record ? "userId='\(userId)' && (status='\(2)' || status='\(3)')"
                                                                        : "userId !='\(userId)' && status='\(1)'"))
        
        completion(resultReqModel)
       

    }
}
