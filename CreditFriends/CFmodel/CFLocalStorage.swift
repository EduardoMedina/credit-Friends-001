//
//  CFLocalStorage.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 09/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import RealmSwift

class LocalStorage {
    
    let realm = try! Realm()
    
    //MARK: agrego una nuevo objeto al almacenamiento interno del dispositivo
    func TTAddObject <T : Object> (_ kObject : T) {
        
        try! realm.write {
            realm.add(kObject)
            NSLog("se agrego un elemento nuevo al storage")
        }
    }
    
    //MARK: Modifico un objeto ya existente en el storage
    func TTUpdateObject <T :  Object> (_ kObject : T) {

        try! realm.write {
            realm.add(kObject, update: true)
            NSLog("se actulizo un elemento del storage")
        }
    }
    //MARK: elimino un objeto ya existente en el storage
    func TTDeleteObject <T :  Object> (_ kObject : T) {
        
        try! realm.write {
            realm.delete(kObject)
            NSLog("se elimino un elemento del storage")
        }
    }
    
    //MARK: elimino todo objeto existente en el storage
    func TTDeleteAllObjects() {
        
        try! realm.write {
            realm.deleteAll()
            NSLog("se eliminaron todos los elementos del storage")
        }
    }
    
    
    
    //MARK: obtengo objetos desde storage | con o sin filtrado
    func TTGetObjectWithPredicate <T : Object> (_ object : T, WithPredicate : Bool,
                                   _ predicate : String) ->  [T] {
        
        var myObject = realm.objects(T.self)
        
        if WithPredicate{
            myObject = realm.objects(T.self).filter(predicate)
        }
        
        var arrayWithObjets : [T] = []
        
        for data in myObject{
            arrayWithObjets.append(data)
        }
        
        return arrayWithObjets
    }
    
    //MARK: Valido que el objecto exista en mi storage
    func TTObjectExists <T : Object>(_ object : T, predicate : String) -> Bool{
        

        let objectResponse = self.TTGetObjectWithPredicate(object,WithPredicate: true, predicate)
        
        guard objectResponse.count > 0  else {
            return false
        }
        
        return true
    }
    
    
    
    //
    func TTCountObjectsSelectedWithPredicate <T : Object> (_ typeObject : T ,WithPredicate:Bool, StringPredicate : String) -> Int {
        
        return self.TTGetAllObjectsSelectedWithPredicate(typeObject, WithPredicate: WithPredicate, StringPredicate: StringPredicate).count
    }
    
    func TTGetAllObjectsSelectedWithPredicate <T : Object> (_ typeObject : T, WithPredicate:Bool, StringPredicate : String) -> [T] {
        return LocalStorage().TTGetObjectWithPredicate(typeObject, WithPredicate: WithPredicate, StringPredicate)
    }
}
