//
//  CFenum.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 09/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation

enum StatusGlobal : String{
    case Successful
    case ResultNotFound
    var status : String{
        switch self {
        case .Successful:
            return "200"
        case .ResultNotFound:
            return "404"
            
        }
    }
    
}

enum FolderPathLibrary {
    case images_Root
    case folder_Root
    
    var folder : String {
        switch self {
        case .images_Root:
            return "FolderCredit/Images/"
        case .folder_Root:
            return "FolderCredit/"
        }
    }
}

enum IdentifiersViewController : String{
    

    case LoginView      = "CFloginViewController"
    case Dashboard      = "CFdashboardViewController"
    case Profile        = "CFProfileViewController"
    case Record         = "CFRecordViewController"
    case Request        = "CFRequestViewController"
    case NewRequest     = "CFNewRequestViewController"
    case ValidateRequest = "CFValidateRequestViewController"
    
    var ToString : String{
        return self.rawValue
    }
}
