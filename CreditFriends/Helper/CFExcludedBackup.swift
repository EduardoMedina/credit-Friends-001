//
//  CFExcludedBackup.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 09/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation

class CFExcludedBackup{
    
    /// MARK Application's Documents directory
    func CFApplicationDocumentsDirectory() -> String{
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first
        
        return documentDirectory!
    }
    
    func CFPathInLibrary(folderName : String) -> String {
        
        let libraryDirectory  = CFApplicationDocumentsDirectory().appending("/\(folderName)")
        
        return libraryDirectory
    }
    
    func CFExcludedFromBackup(dataPath : String) -> Bool {
        //ExcludedFromBackup -- escluir del backup de icloud
        var urlToExclude = URL(fileURLWithPath: dataPath)
        do {
            
            var resourceValues = URLResourceValues()
            resourceValues.isExcludedFromBackup = true
            try urlToExclude.setResourceValues(resourceValues)
            print("Excluding  %@ from backup \(dataPath) ")
            return true
            
        } catch {
            print("failed to set resource value")
            return false
        }
    }
    
    func CFCreateFolderIfNeedInLibrary(dataPath : String) {
        
        
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: dataPath) {
            print("FILE AVAILABLE")
        } else {
            print("FILE NOT AVAILABLE")
            do {
                try fileManager.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Unable to create directory \(error.debugDescription)")
            }
        }
    }
    
}

