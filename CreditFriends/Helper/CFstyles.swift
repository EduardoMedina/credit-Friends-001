//
//  CFstyles.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 09/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import UIKit

enum StyleColors: String{
    case Title_Text_Color       = "626262"
    case SubTitle_Text_Color    = "B3B3B3"
    case Container_Text_Color   = "838383"
    case ColorsApp_Default      = "71835C"
    case ColorsGreen            = "1C666F"
    case ColorsApp_Alternative  = "00b396"
    case BackgroundColor_View   = "F2F2F2"
    case ColorFacebook          = "3b5998"
    case ColorGrayLight         = "B8B8B8"
}
func setCustomColor(kcolor : StyleColors) -> UIColor {
   return #colorLiteral(red: 0, green: 0.6745098039, blue: 0.4, alpha: 1)// setColorHex(hexString: "#" + kcolor.rawValue)
}
// font
enum StyleFont{
    case FontDefaultApp
    case FontAlterntiveApp
    case FontLightApp
    
    
    var FontApp :  String{
        switch self {
        case .FontDefaultApp:
            return "Alvenir Medium"
        case .FontAlterntiveApp:
            return "HelveticaNeue"
        case .FontLightApp:
            return "AvenirNext-Medium"
        }
    }
}


// font
enum StyleFontSize{
    case TitleSize
    case SubTitleSize
    case Extra
    
    var SizeFontApp :  CFloat{
        switch self {
        case  .TitleSize:
            return 18.0
        case .SubTitleSize:
            return 15.0
        case .Extra:
            return 10.0
        }
    }
}
