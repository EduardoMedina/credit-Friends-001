//
//  CFProtocols.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 10/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation

@objc protocol CFNavigationDelegate {
    
    @objc optional func CFPopToViewController()
        
    @objc optional func CFSearchViewController()
    
    @objc optional func CFPushToViewController()
    
    @objc optional func CFDismissViewController()
    
}
