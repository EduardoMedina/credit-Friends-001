//
//  customClass.swift
//  CreditFriends
//
//  Created by Eduardo Medina on 09/06/18.
//  Copyright © 2018 Eduardo Medina. All rights reserved.
//

import Foundation
import UIKit

//MARK: My App delegate
func TTGetDelegate() -> AppDelegate {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    return appDelegate
}

func appRootViewController () -> UIViewController{
    var topVCAl : UIViewController? = nil
    let appRootVC = UIApplication.shared.keyWindow?.rootViewController
    topVCAl = appRootVC
    while ((topVCAl?.presentedViewController) != nil) {
        topVCAl = topVCAl?.presentedViewController;
    }
    return topVCAl!;
}

// MARK: ButtonSubmit
//botton personalizado para el envio de formularios
class ButtonLogin: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customButtonLogin()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customButtonLogin()
    }
    private func customButtonLogin()  {
        self.layer.cornerRadius = 10.0
        self.layer.masksToBounds = true
        self.backgroundColor = setCustomColor(kcolor: .ColorFacebook)
        
    }
}

class ButtonDashboard: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customButtonsDashboard()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customButtonsDashboard()
    }
    private func customButtonsDashboard()  {
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        self.layer.borderColor = setCustomColor(kcolor: .ColorsGreen).cgColor
        self.layer.borderWidth = 1.0
        self.tintColor = setCustomColor(kcolor: .ColorsGreen)
        
    }
}


func CFLayerAnimated(_ layer : CALayer, transitionType : String){
    
    let transition = CATransition()
    transition.duration = 0.3
    transition.type = transitionType
    transition.subtype = kCATransitionFromRight
    layer.add(transition, forKey: kCATransition)
}

//MARK: set button in navigation bar
class CFNavigationsButton {
    
    var navigationDelegate : CFNavigationDelegate?
    
    // set button Close
    func CFCreateButtonClose () -> UIBarButtonItem{
        
        let CloseButton = UIBarButtonItem(image: UIImage(named:"Icon-codigo"),
                                          style: .plain,
                                          target:self,
                                          action: #selector(self.selectorToCloseView))
        
        
        
        return CloseButton
    }
    
    @objc func selectorToCloseView()  {
        let kUserService = UserService()
        kUserService.CloseSession()
    }
    
    
    func CFSetRightBarButtonItems(_ superView : UIViewController) {
        superView.navigationItem .setRightBarButtonItems([CFCreateButtonSearch(),
                                                          CFCreateButtonClose()],
                                                         animated: true)
        superView.automaticallyAdjustsScrollViewInsets = true
    }
    
    private func CFCreateButtonSearch ()-> UIBarButtonItem{
        
        return UIBarButtonItem(barButtonSystemItem: .cancel,
                               target:self,
                               action: #selector(self.selectorToSearchView))
        
    }
    @objc func selectorToSearchView()  {
        navigationDelegate?.CFSearchViewController!()
    }
    
    
    //MARK: Dismiss button -  set to navigation
    func TCFSetDismissButton(_ superView : UIViewController) {
        superView.navigationItem .setLeftBarButtonItems([CFCreateButtonDismiss()],
                                                        animated: true)
        superView.automaticallyAdjustsScrollViewInsets = true
    }
    //MARK: Create Dismiss button -  previous - set to navigation
    private func CFCreateButtonDismiss ()-> UIBarButtonItem{
        let btnDismiss = UIBarButtonItem(image: UIImage(named:"Icon-Close"),
                                         style: .plain,
                                         target:self,
                                         action: #selector(self.selectorToDismissView))
        return btnDismiss
    }
    @objc func selectorToDismissView()  {
        navigationDelegate?.CFDismissViewController!()
    }
    
}

//MARK: set back button in navigation bar
class CFBackButton {
    
    var navigationDelegate : CFNavigationDelegate?

    func CFsetBackButtonInNav (_ superView : UIViewController){
        
        superView.navigationItem.hidesBackButton = true
        
        let backButton = UIBarButtonItem(image: UIImage(named:"back-arrow"), style: .plain, target:self, action: #selector(self.selectorPopToView))
        superView.navigationItem.leftBarButtonItem = backButton
        superView.automaticallyAdjustsScrollViewInsets = true
    }
    @objc func selectorPopToView()  {
        navigationDelegate?.CFPopToViewController!()
    }
}
